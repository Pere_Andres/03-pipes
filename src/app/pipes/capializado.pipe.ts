import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'capializado'
})
export class CapializadoPipe implements PipeTransform {

  transform(value: string, activo: boolean= true): string {
  value = value.toLocaleLowerCase();
  let nombres = value.split(' ');
  if ( activo ){
    nombres = nombres.map( nombre => {
      return nombre[0].toUpperCase() + nombre.substr(1);
    });
    return nombres.join(' ');

  }else{
    nombres[0] = nombres [0][0].toUpperCase() + nombres [0].substr(1);
  }
  return null;
  }

}
