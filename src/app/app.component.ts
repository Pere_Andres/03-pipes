import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre = 'Fernando Costa';
  nombre2 = 'fErnAnDo coSTa';

  videoUrl = 'https://www.youtube.com/embed/0sOjIN-SnWk';
  arreglo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  PI: number = Math.PI;
  porcentaje = 0.234;
  salario = 1234.5;
  idioma = 'es';
  fecha: Date = new Date();
  activar = false;



  valorPromesa = new Promise((resolve => {

    setTimeout(() => {
      resolve('Lego la data');
    });
  }));

  heroe = {
    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 500,
    direccion: {
      calle: 'la que sea',
      // tslint:disable-next-line:align
      numero: '1'
    }
  };

  // tslint:disable-next-line:typedef

  // tslint:disable-next-line:typedef
  cambio(activar: boolean) {

    // tslint:disable-next-line:triple-equals
    if ( activar == true){
      activar = false;
    }else{
      // tslint:disable-next-line:triple-equals
        activar = true;

    }

  }
}
